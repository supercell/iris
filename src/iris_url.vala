/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

namespace Iris {
	public class Url {
		public string scheme;
		public string? host;
		public int? port;
		public string path;
		public string? query;
		public string user;
		public string password;
		public string? fragment;

		internal bool cannot_be_base_url;

		public Url(string scheme = "", string? host = null, int? port = null,
				string path = "", string? query = null, string user = "",
				string password = "", string? fragment = null) {
			this.scheme = scheme;
			this.host = host;
			this.port = port;
			this.path = path;
			this.query = query;
			this.user = user;
			this.password = password;
			this.fragment = fragment;
			this.cannot_be_base_url = false;
		}

		public static Url parse(string raw_url) {
			var parser = new Iris.UrlParser(raw_url);
			return parser.parse();
		}

		/**
		 * Return the serialized string of this Url.
		 *
		 * {{{
		 * var url = Url.parse("https://example.com/path#top");
		 * // Output: https://example.com/path#top
		 * print("%s\n".printf(url.to_string()));
		 *
		 * // Output: https://example.com/path
		 * print("%s\n".printf(uri.to_string(true)));
		 * }}}
		 *
		 * ''Specification:'': [[https://url.spec.whatwg.org/#url-serializing]]
		 *
		 * @param exclude_fragment Should the fragment be included in the string
		 */
		public string to_string(bool exclude_fragment = false) {
			/* Step 1 */
			var output = new StringBuilder(this.scheme + ":");

			/* Step 2 */
			if (this.host != null) {
				/* Step 2.1 */
				output.append("//");

				/* Step 2.2 */
				if (this.includes_credentials()) {
					/* Step 2.2.1 */
					output.append(this.user);

					/* Step 2.2.2 */
					if (this.password != "")
						output.append(":" + this.password);

					/* Step 2.2.3 */
					output.append_c('@');
				}

				/* Step 2.3 */
				/* FIXME: Host Serialization (IPv{4,6}) */
				output.append(this.host);

				/* Step 2.4 */
				if (this.port != null) {
					output.append_c(':');
					output.append(this.port.to_string());
				}
			}
			/* Step 3 */
			else if (this.host != null && this.scheme == "file") {
				output.append("//");
			}

			/* Step 4 */
			if (this.cannot_be_base_url) {
				/* FIXME: Path needs to be a list, when it is append path[0] */
				output.append(this.path);
			}

			/* TODO: Step 5 (more path stuff) */

			/* Step 6 */
			if (this.query != null) {
				output.append("?" + this.query);
			}

			/* Step 7 */
			if (!exclude_fragment && this.fragment != null) {
				output.append("#" + this.fragment);
			}

			/* Step 8 */
			return output.str;
		}

		/**
		 * Returns if the url is special.
		 *
		 * This method can also be used for checking if the {@link Url.scheme}
		 * is special, since the implementation it would use is the same.
		 *
		 * ''Specification:'' [[https://url.spec.whatwg.org/#is-special]]
		 *
		 * @return ``true`` if ``this`` is special, ``false`` otherwise.
		 */
		internal bool is_special() {
			switch (this.scheme) {
				case "ftp":
				case "file":
				case "http":
				case "https":
				case "ws":
				case "wss":
					return true;
				default:
					return false;
			}
		}

		/**
		 * Return the default port for this's {@link Url.scheme}.
		 *
		 * ''Specification:'' [[https://url.spec.whatwg.org/#default-port]]
		 *
		 * ''Note:'' The default port for ``file`` schemes is ``null``.
		 *
		 * @return The default port, or ``null``.
		 */
		internal int? default_port() {
			switch (this.scheme) {
				case "ftp":
					return 21;
				case "file":
					return null;
				case "http":
				case "ws":
					return 80;
				case "https":
				case "wss":
					return 443;
				default:
					return null;
			}
		}

		/**
		 * Returns if this Url contains user credentials.
		 *
		 * ''Specification:'' [[https://url.spec.whatwg.org/#include-credentials]]
		 *
		 * @return ``true`` if either the {@link Url.user} is not empty, or the
		 * {@link Url.password} is not empty.
		 */
		internal bool includes_credentials() {
			return this.user != "" || this.password != "";
		}
	}
}
