/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

namespace Iris {
	public class Window : Gtk.ApplicationWindow {
		/* will be used to adjust the URL based on redirects and such */
		public Gtk.Entry url_entry;
		private Gtk.Box browser_box;

		public Window(Application application) {
			Object(application: application, title: "Iris");
			this.set_default_size(720, 480);

			var main_container = new Gtk.Box(Gtk.Orientation.VERTICAL, 8);
			main_container.set_margin_top(8);

			var toolbar = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 8);

			/* TODO: implement */
			var back_image = new Gtk.Image.from_icon_name("go-previous",
				Gtk.IconSize.SMALL_TOOLBAR);
			var back_button = new Gtk.ToolButton(back_image, null);
			back_button.sensitive = false;

			/* TODO: implement */
			var next_image = new Gtk.Image.from_icon_name("go-next",
				Gtk.IconSize.SMALL_TOOLBAR);
			var next_button = new Gtk.ToolButton(next_image, null);
			next_button.sensitive = false;

			this.url_entry = new Gtk.Entry();
			this.url_entry.set_input_hints(Gtk.InputHints.NO_SPELLCHECK);
			this.url_entry.set_input_purpose(Gtk.InputPurpose.URL);
			this.url_entry.placeholder_text = "Enter a URL...";
			this.url_entry.activate.connect(this.url_entry_activate);

			/* TODO: implement */
			var reload_image= new Gtk.Image.from_icon_name("view-refresh",
				Gtk.IconSize.SMALL_TOOLBAR);
			var reload_button = new Gtk.ToolButton(reload_image, null);
			reload_button.sensitive = false;

			/* TODO: implement */
			var stop_image = new Gtk.Image.from_icon_name("process-stop",
				Gtk.IconSize.SMALL_TOOLBAR);
			var stop_button = new Gtk.ToolButton(stop_image, null);
			stop_button.sensitive = false;

			toolbar.pack_start(back_button, false, false);
			toolbar.pack_start(next_button, false, false);
			toolbar.pack_start(this.url_entry);
			toolbar.pack_start(reload_button, false, false);
			toolbar.pack_start(stop_button, false, false);
			main_container.pack_start(toolbar, false, false);

			this.browser_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
			this.browser_box.set_border_width(10);
			this.browser_box.set_halign(Gtk.Align.START);

			/* TODO: remove later (when homepages are added) */
			string sites = "gemini://gemini.circumlunar.space\ngemini://gus.guru\ngemini://gemini.conman.org";
			var _temp_label = new Gtk.Label(sites);
			_temp_label.selectable = true;
			this.browser_box.pack_start(_temp_label);

			var scrolled_window = new Gtk.ScrolledWindow(null, null);
			scrolled_window.add(this.browser_box);
			main_container.pack_start(scrolled_window);

			this.add(main_container);
			this.show_all();
		}

		private void open_site(string raw_link) {
			/* convert to URI */
			var uri = new Soup.URI(raw_link);
			/* FIXME: again, this is likely because of no scheme */
			if (uri == null) {
				warning("Failed to open link %s", raw_link);
				if (!raw_link.has_prefix("gemini://"))
					stderr.printf("Try adding the `gemini://` scheme\n");
				return;
			}

			/* perform the request */
			Gemini.Response _response = Gemini.make_request(uri);
			string[] _body_lines = _response.body.split("\n");

			/* remove everything in browser_box */
			List<unowned Gtk.Widget> _children = this.browser_box.get_children();
			foreach (var _child in _children)
				this.browser_box.remove(_child);

			var _label = new Gtk.Label("Failed to parse response.");
			_label.wrap = true;
			_label.set_line_wrap_mode(Pango.WrapMode.WORD);
			_label.selectable = true;
			/* TODO: custom setting */
			_label.set_max_width_chars(80);
			/* TODO: rtl support (from the optional lang used in status */
			_label.set_xalign(0);
			_label.activate_link.connect((uri_string) => {
				var _uri = new Soup.URI.with_base(uri, uri_string);
				if (_uri != null) {
					if (_uri.get_scheme() != "gemini")
						return false;
					open_site(_uri.to_string(false));
					this.url_entry.text = _uri.to_string(false);
					return true;
				}
				debug("uri should not be gemini or local: %s", uri_string);
				return false;
			});

			var _builder = new GLib.StringBuilder();
			foreach (unowned string _line in _body_lines) {
				/* provides a fallback */
				string markup = _line;
				markup = Gemini.parse_line(_line);
				_builder.append("%s\n".printf(markup));
			}
			Gemini.reset_parse_mode();
			_label.set_markup(_builder.str);
			this.browser_box.pack_start(_label, false, false);
			this.browser_box.show_all();
		}

		private void url_entry_activate(Gtk.Entry _entry) {
			this.open_site(_entry.text);
		}
	}
}
