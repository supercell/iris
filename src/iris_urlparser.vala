/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

namespace Iris {
	public enum StateOverride {
		NONE,
		SCHEME_START_STATE,
		SCHEME_STATE,
		NO_SCHEME_STATE,
		SPECIAL_RELATIVE_OR_AUTHORITY_STATE,
		PATH_OR_AUTHORITY_STATE,
		RELATIVE_STATE,
		RELATIVE_SLASH_STATE,
		SPECIAL_AUTHORITY_SLASHES_STATE,
		SPECIAL_AUTHORITY_IGNORE_SLASHES_STATE,
		AUTHORITY_STATE,
		HOST_STATE,
		HOSTNAME_STATE,
		PORT_STATE,
		FILE_STATE,
		FILE_SLASH_STATE,
		FILE_HOST_STATE,
		PATH_START_STATE,
		PATH_STATE,
		CANNOT_BE_A_BASE_URL_PATH_STATE,
		QUERY_STATE,
		FRAGMENT_STATE,
	}

	/**
	 * URL Parser class.
	 *
	 * It roughly follows the
	 * [[https://url.spec.whatwg.org/#concept-basic-url-parser|basic URL parser]].
	 *
	 * {{{
	 * var parser = new UrlParser("https://example.com");
	 * var url = parser.parse();
	 * // Output: example.com
	 * print("%s\n", url.host);
	 * }}}
	 */
	public class UrlParser {
		public bool validation_error;

		private long pointer;
		private StateOverride state;
		private StateOverride state_override;
		private StringBuilder buffer;
		private uint8[] input;
		private Url? @base;
		private Url url;

		/* https://url.spec.whatwg.org/#c */
		private uint8 c {
			get {
				return this.input[this.pointer];
			}
		}

		/* https://url.spec.whatwg.org/#remaining */
		private uint8[] remaining {
			get {
				return this.input[this.pointer:this.input.length];
			}
		}

		public UrlParser(string input) {
			this.url = new Url();
			this.validation_error = false;
			this.pointer = 0;
			this.buffer = new StringBuilder();
			this.state = StateOverride.NONE;

			try {
				var _input = trim_control_chars(input);
				this.validation_error = _input == input;
				this.input = _input.data;
			} catch (RegexError e) {
				uint8[] _data = input.data;
				long start = 0;
				long end = _data.length;

				for (start = 0; start < end; start++) {
					uint8 b = _data[start];
					if ((b > 0x1F) && b != 0x20)
						break;
					else
						this.validation_error = true;
				}

				for (end = _data.length; end > 0; end--) {
					uint8 b = _data[end];
					if ((b > 0x1F) && b != 0x20)
						break;
					else
						this.validation_error = true;
				}

				/* TODO: check if the end needs a plus 1 */
				this.input = _data[start:end];
			}
		}

		/**
		 * Basic URL parse from the spec.
		 *
		 * ''Differences:''
		 *
		 *  * if ``url`` is given, a Url is still returned, which is the updated Url.
		 */
		public Url parse(Url? @base = null, Url? url = null,
				StateOverride state_override = StateOverride.NONE) {
			/* used through the different states as a reference */
			this.state_override = state_override;
			/* Step 1
			 * a little different, since the **input** is set when creating a UrlParser,
			 * the input is stripped of the control characters. */
			if (url != null) {
				this.url = url;
			} else {
				this.url = new Url();
			}

			this.validation_error = this.contains_ascii_tab_or_newline();
			this.remove_ascii_tab_or_newline();

			this.state = state_override;
			if (state_override == StateOverride.NONE)
				this.state = StateOverride.SCHEME_START_STATE;

			this.@base = @base;

			while (this.pointer <= this.input.length) {
				debug("State is: %s".printf(this.state.to_string()));
				debug("`c` is %c".printf(this.c));
				/* ``cont`` is used for the times when the spec says 'return' */
				bool cont = true;
				switch (state) {
					case StateOverride.SCHEME_START_STATE:
						cont = this.scheme_start_state();
						break;
					case StateOverride.SCHEME_STATE:
						cont = this.scheme_state();
						break;
					case StateOverride.NO_SCHEME_STATE:
						cont = this.no_scheme_state();
						break;
					case StateOverride.SPECIAL_RELATIVE_OR_AUTHORITY_STATE:
						cont = this.special_relative_or_authority_state();
						break;
					case StateOverride.PATH_OR_AUTHORITY_STATE:
						cont = this.path_or_authority_state();
						break;
					case StateOverride.RELATIVE_STATE:
						cont = this.relative_state();
						break;
					case StateOverride.RELATIVE_SLASH_STATE:
						cont = this.relative_slash_state();
						break;
					case StateOverride.SPECIAL_AUTHORITY_SLASHES_STATE:
						cont = this.special_authority_slashes_state();
						break;
					case StateOverride.SPECIAL_AUTHORITY_IGNORE_SLASHES_STATE:
						cont = this.special_authority_ignore_slashes_state();
						break;
				}
				if (!cont)
					break;
				++this.pointer;
			}

			return this.url;
		}

		/* https://url.spec.whatwg.org/#scheme-start-state */
		private bool scheme_start_state() {
			if (this.is_alpha(this.c)) {
				this.buffer.append_c (this.uint8_to_char(this.c).tolower());
				this.state = StateOverride.SCHEME_STATE;
			} else if (this.state_override == StateOverride.NONE) {
				this.state = StateOverride.NO_SCHEME_STATE;
				--this.pointer;
			} else {
				this.validation_error = true;
				// TODO: throw ?
			}

			return true;
		}

		/* https://url.spec.whatwg.org/#scheme-state */
		private bool scheme_state() {
			/* Step 1 */
			if (this.is_alpha_num(this.c) || this.c == 0x2B /* + */ || this.c == 0x2D /* - */
					|| this.c == 0x2E /* . */) {
				this.buffer.append_c(this.uint8_to_char(this.c).tolower());
			/* Step 2 */
			} else if (this.c == 0x3A /* : */) {
				/* Step 2.1 */
				if (this.state_override != StateOverride.NONE) {
					/* Step 2.1.1 */
					if (this.url.is_special() && !this.is_special_scheme(this.buffer.str)) {
						return false;
					}

					/* Step 2.1.2 */
					if (!this.url.is_special() && this.is_special_scheme(this.buffer.str)) {
						return false;
					}

					/* Step 2.1.3 */
					if (this.url.includes_credentials() || this.url.port != null && buffer.str == "file") {
						return false;
					}

					/* Step 2.1.4 */
					if (this.url.scheme == "file" && (this.url.host == null || this.url.host == "")) {
						return false;
					}
				}

				/* Step 2.2 */
				this.url.scheme = this.buffer.str;

				/* Step 2.3 */
				if (this.state_override != StateOverride.NONE) {
					/* Step 2.3.1 */
					if (this.url.port == this.url.default_port()) {
						this.url.port = null;
					}

					/* Step 2.3.1 */
					return false;
				}

				/* Step 2.4 */
				this.buffer.erase();

				/* Step 2.5 */
				if (this.url.scheme == "file") {
					/* Step 2.5.1 */
					if ((string) this.remaining[0:2] != "//") {
						this.validation_error = true;
					}

					/* Step 2.5.2 */
					this.state = StateOverride.FILE_STATE;
				}
				/* Step 2.6 */
				else if (this.url.is_special() && this.@base != null
						&& this.@base.scheme == this.url.scheme) {
					this.state = StateOverride.SPECIAL_RELATIVE_OR_AUTHORITY_STATE;
				}
				/* Step 2.7 */
				else if (this.url.is_special()) {
					this.state = StateOverride.SPECIAL_AUTHORITY_SLASHES_STATE;
				}
				/* Step 2.8 */
				else if (this.remaining[0] == 0x2F /* / */) {
					this.state = StateOverride.PATH_OR_AUTHORITY_STATE;
					++this.pointer;
				}
				/* Step 2.9 */
				else {
					this.url.cannot_be_base_url = true;
					this.url.path = this.url.path + "";
					this.state = StateOverride.CANNOT_BE_A_BASE_URL_PATH_STATE;
				}
			}
			/* Step 3 */
			else if (this.state_override == StateOverride.NONE) {
				this.buffer.erase();
				this.state = StateOverride.NO_SCHEME_STATE;
				/* pointer is increased at the end of the while loop */
				this.pointer = -1;
			}
			/* Step 4 */
			else {
				this.validation_error = true;
				// TODO: throw?
			}

			return true;
		}

		/* https://url.spec.whatwg.org/#no-scheme-state */
		private bool no_scheme_state() {
			if (this.@base == null || (this.@base.cannot_be_base_url && this.c != 0x23 /* # */)) {
				this.validation_error = true;
				// TODO: throw?
			} else if (this.@base.cannot_be_base_url && this.c == 0x23 /* # */) {
				this.url.scheme = this.@base.scheme;
				this.url.path = this.@base.path;
				this.url.query = this.@base.query;
				this.url.fragment = "";
				this.url.cannot_be_base_url = true;
				this.state = StateOverride.FRAGMENT_STATE;
			} else if (this.@base.scheme != "file") {
				this.state = StateOverride.RELATIVE_STATE;
				--this.pointer;
			} else {
				this.state = StateOverride.FILE_STATE;
				--this.pointer;
			}

			return true;
		}

		/* https://url.spec.whatwg.org/#special-relative-or-authority-state */
		private bool special_relative_or_authority_state() {
			/* Step 1 */
			if (this.c == 0x2F /* / */ && this.remaining[0] == 0x2F /* / */) {
				this.state = StateOverride.SPECIAL_AUTHORITY_IGNORE_SLASHES_STATE;
				++this.pointer;
			/* Step 2 */
			} else {
				this.validation_error = true;
				this.state = StateOverride.RELATIVE_STATE;
				--this.pointer;
			}

			return true;
		}

		/* https://url.spec.whatwg.org/#path-or-authority-state */
		private bool path_or_authority_state() {
			/* Step 1 */
			if (this.c == 0x2F /* / */) {
				this.state = StateOverride.AUTHORITY_STATE;
			/* Step 2 */
			} else {
				this.state = StateOverride.PATH_STATE;
				--this.pointer;
			}

			return true;
		}

		/* https://url.spec.whatwg.org/#relative-state */
		private bool relative_state() {
			/* Step 1 */
			this.url.scheme = this.@base.scheme;

			/* Step 2 */
			if (this.c == 0x2F /* / */) {
				this.state = StateOverride.RELATIVE_SLASH_STATE;
			/* Step 3 */
			} else if (this.url.is_special() && this.c == 0x5C /* \ */) {
				this.validation_error = true;
				this.state = StateOverride.RELATIVE_SLASH_STATE;
			/* Step 4 */
			} else {
				/* Step 4.1 */
				this.url.user = this.@base.user;
				this.url.password = this.@base.password;
				this.url.host = this.@base.host;
				this.url.port = this.@base.port;
				/* FIXME: currently path is just a string */
				this.url.path = this.@base.path;
				this.url.query = this.@base.query;

				/* Step 4.2 */
				if (this.c == 0x3F /* ? */) {
					this.url.query = "";
					this.state = StateOverride.QUERY_STATE;
				}
				/* Step 4.3 */
				else if (this.c == 0x23 /* # */) {
					this.url.fragment = "";
					this.state = StateOverride.FRAGMENT_STATE;
				}
				/* Step 4.4 */
				/* the uint8[] isn't 0-terminaled, so we can't check for 0x0 */
				else if (this.pointer < this.input.length) {
					this.url.query = null;
					/* TODO: remove last (if any) from this.url.path */
					this.state = StateOverride.PATH_STATE;
					--this.pointer;
				}
			}

			return true;
		}

		/* https://url.spec.whatwg.org/#relative-slash-state */
		private bool relative_slash_state() {
			/* Step 1 */
			if (this.url.is_special() && (this.c == 0x2F /* / */ || this.c == 0x5C /* \ */)) {
				/* Step 1.1 */
				if (this.c == 0x5C /* / */)
					this.validation_error = true;
				/* Step 1.2 */
				this.state = StateOverride.SPECIAL_AUTHORITY_IGNORE_SLASHES_STATE;
			}
			/* Step 2 */
			else if (this.c == 0x2F /* / */) {
				this.state = StateOverride.AUTHORITY_STATE;
			}
			/* Step 3 */
			else {
				this.url.user = this.@base.user;
				this.url.password = this.@base.password;
				this.url.host = this.@base.host;
				this.url.port = this.@base.port;
				this.state = StateOverride.PATH_STATE;
				--this.pointer;
			}
			return true;
		}

		/* https://url.spec.whatwg.org/#relative-slash-state */
		private bool special_authority_slashes_state() {
			/* Step 1 */
			if (this.c == 0x2F /* / */ && this.remaining[0] == 0x2F /* / */) {
				this.state = StateOverride.SPECIAL_AUTHORITY_IGNORE_SLASHES_STATE;
				++this.pointer;
			}
			/* Step 2 */
			else {
				this.validation_error = true;
				this.state = StateOverride.SPECIAL_AUTHORITY_IGNORE_SLASHES_STATE;
				--this.pointer;
			}
			return true;
		}

		/* https://url.spec.whatwg.org/#relative-slash-state */
		private bool special_authority_ignore_slashes_state() {
			/* Step 1 */
			if (this.c != 0x2F /* / */ && this.c != 0x5C /* \ */) {
				this.state = StateOverride.AUTHORITY_STATE;
				--this.pointer;
			}
			/* Step 2 */
			else {
				this.validation_error = true;
			}

			return true;
		}

		private string trim_control_chars(string input) throws RegexError {
			Regex reg = /^[\x00-\x1F\x20]+|[\x00-\x1F\x20]+$/;
			return reg.replace(input, input.length, 0, "");
		}

		private bool is_special_scheme(string scheme) {
			switch (scheme) {
				case "ftp":
				case "file":
				case "http":
				case "https":
				case "ws":
				case "wss":
					return true;
				default:
					return false;
			}
		}

		private bool contains_ascii_tab_or_newline() {
			/* seems that regex literals don't throw errors? */
			Regex reg = /\x09|\x0A|\x0D/;
			return reg.match((string) this.input);
		}

		private void remove_ascii_tab_or_newline() {
			try {
				Regex reg = /\x09|\x0A|\x0D/;
				string _input = reg.replace((string) this.input, this.input.length, 0, "");
				this.input = _input.data;
			} catch (RegexError e) {
				string _input = (string) this.input;
				_input.replace("\t", "");
				_input.replace("\n", "");
				_input.replace("\r", "");
				this.input = _input.data;
			}
		}

		private bool is_alpha(uint8 b) {
			return (0x41 /* A */ <= b && b <= 0x5A /* Z */)
				|| (0x61 /* a */ <= b && b <= 0x7A /* z */);
		}

		private bool is_alpha_num(uint8 b) {
			return is_alpha(b) || (0x30 /* 0 */ <= b && b <= 0x39 /* 9 */);
		}

		private char uint8_to_char(uint8 b) {
			return (char) b;
		}
	}
}
