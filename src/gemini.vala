/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

namespace Gemini {
	/**
	 * Maximum number of bytes in a URL that Gemini servers will accept.
	 */
	public const int MAX_URL_LENGTH = 1024;
	/**
	 * Maximum ``<META>`` length in bytes.
	 *
	 * Clients **should** close the connection if the number of bytes in
	 * ``<META>`` exceeds this, and inform the user.
	 */
	public const int MAX_META_LENGTH = 1024;

	/* TODO: remove this when a parser class is created */
	private bool parsing_preformatted = false;

	/**
	 * Gemini response status codes.
	 *
	 * Gemini uses two-digit numeric status codes. Related status codes share
	 * the same first digit.
	 */
	public enum StatusCode {
		/**
		 * The requested resource accepts a line of textual user input.
		 *
		 * The ``<META>`` line is a prompt which should be displayed to the
		 * user. The same resource should then be requested again with the
		 * user's input included as a query component. Queries are included in
		 * requests as per the usual generic URL definition in RFC3986, i.e.
		 * separated from the path by a ``?``. Reserved characters used in the
		 * user's input ''must'' be "percent-encoded" as per RFC3986, and space
		 * characters should also be percent-encoded.
		 */
		INPUT = 10,
		/**
		 * As per status code {@link StatusCode.INPUT}, but for use with
		 * sensitive input such as passwords.
		 *
		 * Clients should present the prompt as per {@link StatusCode.INPUT},
		 * but the user's input should not be echoed to the screen to prevent it
		 * being read by "shoulder surfers".
		 */
		INPUT_SENSITIVE = 11,
		/**
		 * The request was handled successfully and a response body will follow
		 * the response header.
		 *
		 * The ``<META>`` line is a MIME media type which applies to the
		 * response body.
		 */
		SUCCESS = 20,
		/**
		 * The server is redirecting the client to a new location for the
		 * requested resource.
		 *
		 * There is no response body. ``<META>`` is a new URL for the requested
		 * resource. The URL may be absolute or relative. The redirect should be
		 * considered temporary, i.e. clients should continue to request the
		 * resource at the original address and should not perform convenience
		 * actions like automatically updating bookmarks.
		 */
		REDIRECT_TEMPORARY = 30,
		/**
		 * The requested resource should be consistently requested from the new
		 * URL provided in future.
		 */
		REDIRECT_PERMANENT = 31,
		/**
		 * The request failed.
		 *
		 * There is no response body. The nature of the failure is temporary,
		 * i.e. an identical request **may** succeed in the future. The contents
		 * of ``<META>`` may provide additional information on the failure, and
		 * should be displayed to human users.
		 */
		TEMPORARY_FAILURE = 40,
		/**
		 * The server is unavailable due to overload or maintenance.
		 *
		 * This is similar to the
		 * [[https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/503|HTTP 503 response]]
		 * [developer.mozilla.org].
		 */
		SERVER_UNAVAILABLE = 41,
		/**
		 * A CGI process, or similar system for generating dynamic content, died
		 * unexpectedly or timed out.
		 */
		CGI_ERROR = 42,
		/**
		 * A proxy request failed because the server was unable to successfully
		 * complete a transaction with the remote host.
		 *
		 * This is similar to the
		 * [[https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/502|HTTP 502 response]]
		 * [developer.mozilla.org] and the
		 * [[https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/504|HTTP 504 response]]
		 * [developer.mozilla.org].
		 */
		PROXY_ERROR = 43,
		/**
		 * Rate limiting is in effect.
		 *
		 * ``<META>`` is an integer number of seconds which the client must wait
		 * before another request is made to this server.
		 *
		 * This is similar to the
		 * [[https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/429|HTTP 429 response]]
		 * [developer.mozilla.org].
		 */
		SLOW_DOWN = 44,
		/**
		 * The request failed.
		 *
		 * There is no response body. The nature of the request is permanent,
		 * i.e. identical future requests will reliably fail for the same
		 * reason. The contents of ``<META>`` may provide additional information
		 * on the failure, and should be displayed to human users. Automatic
		 * clients such as aggregators or indexing crawlers should not repeat
		 * this request.
		 */
		PERMANENT_FAILURE = 50,
		/**
		 * The requested resource could not be found but may be available in the
		 * future.
		 *
		 * This is similar to the
		 * [[https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/404|HTTP 404 response]]
		 * [developer.mozilla.org].
		 */
		NOT_FOUND = 51,
		/**
		 * The resource requested is no longer available and will not be
		 * available again.
		 *
		 * Search engines and similar tools should remove this resource from
		 * their indices. Content aggregators should stop requesting the
		 * resource and convey to their human users that the subscribed
		 * resource is gone.
		 *
		 * This is similar to the
		 * [[https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/410|HTTP 410 response]]
		 * [developer.mozilla.org].
		 */
		GONE = 52,
		/**
		 * The request was for a response at a domain not served by the server
		 * and the server does not accept proxy requests.
		 */
		PROXY_REQUEST_REFUSED = 53,
		/**
		 * The server was unable to parse the client's request, presumably due
		 * to a malformed request.
		 *
		 * This is similar to the
		 * [[https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/400|HTTP 400 response]]
		 * [developer.mozilla.org].
		 */
		BAD_REQUEST = 59,
		/**
		 * The requested resource requires a client certificate to access.
		 *
		 * If the request was made without a certificate, it should be repeated
		 * with on. If the request was made with a certificate, the server did
		 * not accept it and the request should be repeated with a different
		 * certificate. The contents of ``<META>`` (and/or the specific 6x code)
		 * may provide additional information on certificate requirements or the
		 * reason a certificate was rejected.
		 */
		CLIENT_CERTIFICATE_REQUIRED = 60,
		/**
		 * The supplied certificate is not authorised for accessing the
		 * particular requested resource.
		 *
		 * The problem is not with the certificate itself, which may be
		 * authorised for other resources.
		 */
		CERTIFICATE_NOT_AUTHORISED = 61,
		/**
		 * The supplied client certificate was not accepted because it is not
		 * valid.
		 *
		 * This indicates a problem with the certificate in and of itself, with
		 * no consideration of the particular requested resource. The most
		 * likely cause is that the certificate's validity start date is in the
		 * future or its expiry date has passed, but this code may also indicate
		 * an invalid signature, or a violation of a ``X.509`` standard
		 * requirements. The ``<META>`` should provide more information about
		 * the exact error.
		 */
		CERTIFICATE_NOT_VALID = 62,
	}

	public class Response {
		public ResponseHeader response_header;
		public string body;

		/* TODO: should probably be async in some way */
		public Response(DataInputStream input_stream) throws ResponseError {
			this.response_header = {};
			size_t bytes_read = 0;
			uint8[] status_buffer = new uint8[2];

			try {
				bytes_read = input_stream.read(status_buffer);
			} catch (IOError e) {
				throw new ResponseError.READING_STREAM(e.message);
			}

			/* early EOF */
			/* TODO: optional lenient since the spec. says SHOULD (not MUST),
			 * however, this would only apply for single digits since the
			 * buffer size is two. */
			if (bytes_read != 2)
				throw new ResponseError.INVALID_STATUS("Status is not two characters");

			this.response_header.status = int.parse((string) status_buffer);
			debug("response_header.status = %d", this.response_header.status);

			/* FIXME: lenient mode for tab? gus.guru uses a tab (0x9), however,
			 * the spec has 0x20 (space). */
			uint8 space = 0x0;
			try {
				space = input_stream.read_byte();
			} catch (IOError e) {
				throw new ResponseError.READING_STREAM(e.message);
			}

			if (space != 0x20)
				throw new ResponseError.INVALID_SPACE(
					"Expected 0x20, received: 0x%x".printf(space));

			try {
				this.response_header.meta = input_stream.read_upto("\r\n", 2,
					out bytes_read);
				/* skip \r\n */
				input_stream.skip(2);
			} catch (IOError e) {
				throw new ResponseError.READING_STREAM(e.message);
			}
			var body = new StringBuilder();

			try {
				string line = null;
				while ((line = input_stream.read_line(null)) != null) {
					body.append(line);
					body.append_c('\n');
				}
			} catch (IOError e) {
				throw new ResponseError.READING_STREAM(e.message);
			}

			this.body = body.str;
		}
	}

	public struct ResponseHeader {
		int status;
		string meta;
	}

	public errordomain ResponseError {
		READING_STREAM,
		INVALID_STATUS,
		INVALID_SPACE,
		INVALID_META,
	}

	public errordomain RequestError {
		RESOLVING_ADDRESS,
		CONNECTING,
		READING_RESPONSE,
		CREATING_RESPONSE,
	}

	/**
	 * Performs a TLS connection to uri's host. Returns the body of the response
	 * or a string containing an error message.
	 *
	 * {{{
	 * List<string> response = make_request("gemini://gemini.circumlunar.space");
	 * foreach (string line in response)
	 *     print("%s\n", line);
	 * // # Project Gemini
	 * //
	 * // ## Overview
	 * // [response trimmed]
	 * }}}
	 *
	 * ''Known Bugs:''
	 *
	 *  * uses the sync methods over async, freezed iris each time a new
	 *    request is made.
	 *  * assumes a status code of in the ``2X`` range.
	 *  * only supports port ``1965``.
	 *  * does not store certificates.
	 *  * certificate handling may not be the best in general.
	 *
	 * @param uri The parsed Soup.URI
	 * @return the body of the request.
	 *
	 */
	public Response make_request(Soup.URI uri) throws RequestError {
		/* resolve IP from host string */
		GLib.Resolver resolver = GLib.Resolver.get_default();
		List<GLib.InetAddress> addresses;
		try {
			/* TODO: try NetworkAddress */
			/* TODO: try lookup_by_name_async */
			addresses = resolver.lookup_by_name(uri.get_host(), null);
		} catch (Error e) {
			debug("URI: %s", uri.to_string(false));
			debug("Host: %s", uri.get_host());
			throw new RequestError.RESOLVING_ADDRESS(e.message);
		}
		InetAddress address = addresses.nth_data(0);

		/* create a socket client */
		var client = new GLib.SocketClient();
		client.tls = true;
		/* TODO: TOFU */
		client.tls_validation_flags = TlsCertificateFlags.GENERIC_ERROR;
		SocketConnection conn;
		try {
			/* TODO: try connect_async */
			/* TODO: cast port from uri, if it's not 0 */
			conn = client.connect(new GLib.InetSocketAddress(address, 1965));
		} catch (Error e) {
			debug("URI: %s", uri.to_string(false));
			debug("Address: %s - %s", address.to_string(), e.message);
			throw new RequestError.CONNECTING(e.message);
		}

		if (uri.get_scheme() == null || uri.get_scheme() == "")
			uri.set_scheme("gemini");
		/* libsoup has a fallback path of '/' with HTTP(S) URIS, this doesn't
		 * appear to be the case for other schemes. */
		/* TODO: use the 3X status codes for redirects instead of just adding
		 * the '/' ? */
		if (uri.get_path() == null || uri.get_path() == "")
			uri.set_path("/");

		var message = new GLib.StringBuilder();
		message.append("%s://".printf(uri.get_scheme()));
		message.append(uri.get_host());
		message.append(uri.get_path());
		message.append("\r\n");
		debug("URI Path: %s".printf(uri.get_path()));

		try {
			conn.output_stream.write(message.str.data);
		} catch (IOError e) {
			debug("Reading Response: %s", e.message);
			throw new RequestError.READING_RESPONSE(e.message);
		}

		try {
			return new Response(new DataInputStream(conn.input_stream));
		} catch (ResponseError e) {
			throw new RequestError.CREATING_RESPONSE(e.message);
		}
	}

	/**
	 * Parse a string and return the Pango markup required to display it.
	 *
	 * Currently supported:
	 *
	 *  * Headings (all levels)
	 *  * Links (though you can't tell the difference between protocols yet)
	 *  * Lists
	 *  * Preformatted
	 *
	 * For now you will need to call {@link reset_parse_mode} afterwards so the
	 * parsing mode is reset (this is something that will be addressed soon).
	 *
	 * {{{
	 * string heading1 = "# Heading 1";
	 * string parsed = parse_line(heading1);
	 * print("%s\n", parsed);
	 * // output: <span font="17">Heading 1</span>
	 *
	 * string heading2 = "## Heading 2";
	 * string parsed2 = parse_line(heading2);
	 * print("%s\n", parsed2);
	 * // output: <big>Heading 2</big>
	 *
	 * string heading3 = "### Heading 3";
	 * string parsed3 = parse_line(heading3);
	 * print("%s\n", parsed3);
	 * // output: <span font_weight="bold">Heading 3</span>
	 * }}}
	 *
	 * @param _line The gemtext to parse
	 * @return The string in pango markup format if applicable to the line.
	 */
	public string parse_line(string _line) {
		/* ''blank lines are rendered by clients verbatim, i.e. if you put two
		 * or three blank lines between your paragraphs, the reader will see
		 * two or three blank lines. */
		if (_line == "") {
			return _line;
		}

		/* preformatted */
		if (_line.has_prefix("```")) {
			Gemini.parsing_preformatted = !Gemini.parsing_preformatted;
			debug("changed preformatted mode to: %s",
				Gemini.parsing_preformatted.to_string());
			if (Gemini.parsing_preformatted)
				return "<tt>";
			else
				return "</tt>";
		}
		if (Gemini.parsing_preformatted) {
			return GLib.Markup.escape_text(_line);
		}

		/* links */
		/* start with the two characters `=>` */
		if (_line.has_prefix("=>")) {
			debug("link: %s", _line);
			var builder = new StringBuilder();
			/* followed by optional whitespace (spaces or tabs, as many or few
			 * as you like) */
			var matches = GLib.Regex.split_simple("^=>[\\s\\t]*(\\S+)[\\s\\t]*(.*)?$",
				_line);
			/* if it doesn't match anything for some reason, just return the
			 * line. note that for some reason the regex pads with empty
			 * matches. */
			if (matches.length < 2)
				return _line;

			string _escaped_link = GLib.Markup.escape_text(matches[1]);
			builder.append("<a href=\"");
			builder.append(_escaped_link);
			if (matches[2] != "") {
				string _escaped_title = GLib.Markup.escape_text(matches[2]);
				builder.append("\" title=\"%s\">".printf(_escaped_title));
				builder.append(matches[2]);
			} else {
				builder.append("\">");
				builder.append(_escaped_link);
			}
			builder.append("</a>");
			return builder.str;
		}

		/* headings */
		/* gemini supports three levels of headings */
		if (_line.has_prefix("#")) {
			debug("heading: %s", _line);
			var builder = new StringBuilder();
			/* headings start with one, two or three # symbols followed by a
			 * mandatory space character. */
			var matches = GLib.Regex.split_simple("^(#{1,3})\\s+(.*)$", _line);
			/* again, the regex returns extras */
			if (matches.length <= 3) {
				debug("heading: matches.length <= 3: '%s'",
					(string) matches);
				return _line;
			}
			/* not sure if there is a way to better way of creating larger font
			 * sizes */
			switch (matches[1].length) {
				case 1:
					builder.append("<span font=\"17\">%s</span>".printf(matches[2]));
					break;
				case 2:
					builder.append("<big>%s</big>".printf(matches[2]));
					break;
				case 3:
					builder.append("<span font_weight=\"bold\">%s</span>".printf(matches[2]));
					break;
				default:
					builder.append(_line);
					break;
			}
			return builder.str;
		}

		/* lists */
		/* gemini supports unorded lists */
		/* XXX: gemini.conman.org has a line '* * * * *' whichs is interpreted
		 * as a list. there is a fix by changing how the lines are read from
		 * the request stream, but it usually doesn't look correct for serif
		 * and sans-serif fonts. so it's likely this will be a custom setting
		 * when that's implemented. another option may be  to .strip() the line
		 * after the parsing it as markup, i.e. the last return in this
		 * function. */
		if (_line.has_prefix("*")) {
			debug("list: %s", _line);
			string line = GLib.Markup.escape_text(_line.strip());
			return "\u2022 %s".printf(line[1:line.length]);
		}
		return GLib.Markup.escape_text(_line);
	}

	/**
	 * Set the internal parsing mode back to the default (not in preformatted
	 * mode).
	 */
	public void reset_parse_mode() {
		Gemini.parsing_preformatted = false;
	}
}
