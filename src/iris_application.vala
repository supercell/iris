/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

namespace Iris {
	public class Application : Gtk.Application {
		public Application() {
			/* TODO: use NON_UNIQUE since multiple windows should be allowed */
			Object(application_id: "org.neocities.yume-neru.iris",
				flags: GLib.ApplicationFlags.FLAGS_NONE);
		}

		protected override void activate() {
			new Window(this);
		}
	}
}
